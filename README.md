# tag-buffer-name

***A very simple way to add tags to Emacs buffers name***

This package provides **`tag-buffer-name'** functions to add **tags** in front of buffer name.

Use **tag-buffer-name** to add a tag in front of the buffer name,
**untag-buffer-name** to remove all tags from buffer name and
**re-tag-buffer-name** to add again the current tag of a buffer name.  
Adding an empty tag remove all the current tags.

By default buffer name tags are enclosed between ! and : characters.

So tagging the buffer *test.el* with the tag '*main*' will rename it in *!main: test.el*

The mechanism allow to group buffers by tags in Buffer menu like ibuffer, helm or ivy.

## FAQ

### Adding tags automatically when loading a file

	(defun my-auto-tag-buffer-name-hook ()
	  (interactive)
	  (let* ((file-name (buffer-file-name)))
		(cl-labels ((fm (str)
					  (string-match str  file-name)))
		  (let ((tag (cond ((fm "wheebuild.*/home/.*/src/controller")   "wc")
						   ((fm "wheebuild.*/home/.*/src/uws")          "wu")
						   ((fm "jessbuild.*/home/.*/src/controller")   "jc")
						   ((fm "jessbuild.*/home/.*/src/uws")          "ju")
						   ((fm "home/.*/src/controller")               "lc")
						   ((fm "home/.*/src/uws")                      "lu")
						   ((fm "home/.*/src/digilab-back")             "db")
						   ((fm "home/.*/src/digilab-front")            "df"))))
			(when tag
			  (tag-buffer-name tag))))))

	(add-hook 'find-file-hook 'my-auto-tag-buffer-name-hook)

### Using buffer name tags with purpose

	;; Purpose
	(require 'ivy-purpose)
	(ivy-purpose-setup)

	(setq purpose-user-regexp-purposes nil)

	(add-to-list 'purpose-user-regexp-purposes '("^!wc: " . Wheebuild-Controller))
	(add-to-list 'purpose-user-regexp-purposes '("^!wu: " . Wheebuild-UWS))
	(add-to-list 'purpose-user-regexp-purposes '("^!jc: " . Jessbuild-Controller))
	(add-to-list 'purpose-user-regexp-purposes '("^!ju: " . Jessbuild-UWS))
	(add-to-list 'purpose-user-regexp-purposes '("^!lc: " . Local-Controller))
	(add-to-list 'purpose-user-regexp-purposes '("^!lu: " . Local-UWS))
	(add-to-list 'purpose-user-regexp-purposes '("^!db: " . Digilab-Back))
	(add-to-list 'purpose-user-regexp-purposes '("^!df: " . Digilab-front))

	(purpose-compile-user-configuration) ; activates your changes

### Preserve tags with EXWM

	(defun exwm-rename-buffer-current-title ()
	  (interactive)
	  (exwm-workspace-rename-buffer (format "=%s= %s: %s" exwm-workspace-current-index exwm-class-name exwm-title))
	  (re-tag-buffer-name))

	(add-hook 'exwm-update-title-hook 'exwm-rename-buffer-current-title)
