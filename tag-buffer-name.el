;;; tag-buffer-name.el --- A very simple way to add tags to emacs buffers names -*- lexical-binding: t -*-

;; Copyright (C) 2018-2018  Philippe Brochard

;; Author: Philippe Brochard <hocwp@free.fr>
;; URL: https://bitbucket.org/pbrhocwp/tag-buffer-name
;; Version: 0.0.1
;; Package-Requires: ((emacs "24.1"))
;; Keywords: matching

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides `tag-buffer-name' functions to add tags in
;; front of buffer name.
;;
;; Use tag-buffer-name to add a tag in front of the buffer name,
;; untag-buffer-name to remove all tags from buffer name and
;; re-tag-buffer-name to add again the current tag of a buffer name.
;; Adding an empty tag remove all the current tags.

;;; Code:

(require 'cl-seq)

;;* Customization
(defgroup tag-buffer-name nil
  "A very simple way to add tags to emacs buffers name."
  :group 'convenience)

(defcustom tag-buffer-name-prefix "!"
  "A string to represent the beginning of a tag"
  :type 'string)

(defcustom tag-buffer-name-suffix ":"
  "A string to represent the end of a tag"
  :type 'string)

(defvar tag-buffer-alist '()
  "An alist of all tag associated to buffers name")

(defvar tag-buffer-name-regexp "^!.*?: ")

(defun recompute-tag-buffer-name-vars (symbol newval operation where)
  (setf tag-buffer-name-regexp (format "^%s.*?%s "
									   (if (eq symbol 'tag-buffer-name-prefix)
										   newval
										 tag-buffer-name-prefix)
									   (if (eq symbol 'tag-buffer-name-suffix)
										   newval
										 tag-buffer-name-suffix))))

(add-variable-watcher 'tag-buffer-name-prefix 'recompute-tag-buffer-name-vars)
(add-variable-watcher 'tag-buffer-name-suffix 'recompute-tag-buffer-name-vars)

(defun clean-tag-buffer-alist ()
  "Clean killed buffers from the tag buffer list"
  (interactive)
  (setq tag-buffer-alist (cl-remove-if (lambda (b)
										 (null (buffer-name (car b))))
									   tag-buffer-alist)))

(defun current-tag-buffer-name ()
  "Return the tag associated to the current buffer"
  (alist-get (current-buffer) tag-buffer-alist))

(defun untag-buffer-name ()
  "Untag a buffer name"
  (interactive)
  (clean-tag-buffer-alist)
  (setq tag-buffer-alist
		(delq (assoc (current-buffer) tag-buffer-alist) tag-buffer-alist))
  (let ((buffer-name (buffer-name)))
	(when (string-match tag-buffer-name-regexp buffer-name)
	  (rename-buffer (replace-regexp-in-string tag-buffer-name-regexp
											   "" buffer-name))
	  t)))

(defun tag-buffer-name (tag)
  "Tag a buffer name"
  (interactive (list
				(read-string (format "Buffer name tag: ")
							 (or (current-tag-buffer-name) "")
							 nil nil (or (current-tag-buffer-name) ""))))
  (untag-buffer-name)
  (when (and tag (not (string= tag "")))
	(setf (alist-get (current-buffer) tag-buffer-alist) tag)
	(rename-buffer (format "%s%s%s %s"
						   tag-buffer-name-prefix
						   tag
						   tag-buffer-name-suffix
						   (buffer-name))
				   t)))

(defun re-tag-buffer-name ()
  "Retag a buffer name with its current tag"
  (interactive)
  (tag-buffer-name (current-tag-buffer-name)))


(provide 'tag-buffer-name)

;;; tag-buffer-name.el ends here
